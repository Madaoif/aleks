import MySQLdb
from peewee import *
import time, datetime
import re
import json
# from bson import json_util
import string
import random

db = MySQLDatabase('alex1', user='root', passwd='makbet')

class BaseModel(Model):
    class Meta:
        database = db

class Alex(BaseModel): 
    price = DecimalField()
    name = CharField(max_length = 500)
    created_date = DateTimeField(default=datetime.datetime.now)
    site_link = CharField(max_length = 500)
    serialization = TextField()
    name_with_digits = CharField(max_length = 500)

db.connect()                              #connection to database 
db.create_tables([Alex], safe=True)       #creation table named Alex


class AlexanderPipline(object): #function to write objects to database

    jsn = {}
    random_choice = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))  #generation a random string for key in dict


    def process_item(self, item, spider):
        query = Alex.select().where(Alex.name == item['name'], Alex.site_link == item['site_link'], Alex.name_with_digits == item['name_with_digits']).first()

        print query

        try:
            print 1
            query = Alex.select().where(Alex.name == item['name'], Alex.site_link == item['site_link'], Alex.price == item['price'], Alex.name_with_digits == item['name_with_digits']).first()
            print query.price
            self.jsn = json.loads(query.serialization)            
            self.jsn[self.random_choice] = item['price']
            query.serialization = json.dumps(self.jsn)         
            query.save()
            if query.exists():
                print "query alredy exists"

        except:
            
            try:
                print 2
                query = Alex.select().where(Alex.name == item['name'], Alex.site_link == item['site_link'], Alex.name_with_digits == item['name_with_digits']).first()
                self.jsn = json.loads(query.serialization)
                self.jsn[self.random_choice] = item['price']
                query.serialization =  json.dumps(self.jsn)
                query.price = item['price']
                query.save()

            except:  
                print 3
                self.jsn = {self.random_choice:item['price']}
                jsn_serialized = json.dumps(self.jsn)    
                query = Alex.select().where(Alex.name == item['name'] , Alex.site_link == item['site_link'], Alex.price == item['price'], Alex.name_with_digits == item['name_with_digits']) #checking if query with specific name exists
                if query.exists():
                    print "query alredy exists"
                else:
                    ax = Alex(price = int(item['price']), name = item['name'].lower(), site_link = item['site_link'], serialization = jsn_serialized, name_with_digits= item['name_with_digits']).save()
        return item

