# coding: utf-8
import requests
import os
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector, Selector
from parse2.items import AlexandrDrmax
import lxml.html
#from BeautifulSoup import BeautifulSoup
import re


class Apotek(CrawlSpider):

	name='apotek'

	allowed_domains = ['www.apotek.cz']

	start_urls = [

			"http://www.apotek.cz/zdravi",
			"http://www.apotek.cz/kosmetika",
			"http://www.apotek.cz/deti-a-maminky",
			"http://www.apotek.cz/veterinarni-pece",
			"http://www.apotek.cz/specialni-vyziva",
			"http://www.apotek.cz/sportovni",
			"http://www.apotek.cz/drogerie",

	]

	def parse(self, response):
		#500
		for page in range(100):
			link = response.url +'?price_to=11500&page='+ str(page)
			request = Request(link, callback = self.parse_page)
			yield request
	def parse_page(self, response):
		item = AlexandrDrmax()
		hxs = HtmlXPathSelector(response)
		price = hxs.xpath("//div[@class='product__price']/text()").extract()
		name = hxs.xpath("//div[@class='valign']/h2[@class='product__title beta']/text()").extract()
		model = hxs.xpath("//li[@class='product']/a/p/text()").extract()
		for p,n,m in zip(price, name, model):
			p = p.strip()
			p = int(''.join([i for i in p if i.isdigit()]))
			n = n + " " + m
			#
			pattern = re.compile("(\d+)")
			d=re.sub(pattern,r' \1 ', n)
			d=[int(s) for s in d.split() if s.isdigit()]
			d= ' '.join(map(str, d))
			#print "Digit",d

			item['name_with_digits']=d.decode( 'unicode-escape' )
			#item['name_with_digits']=d
			item['price'] = p
			item['name'] = n
			item['site_link'] = self.allowed_domains[0]
			yield item