# coding: utf-8
import requests
import os
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector, Selector
from parse2.items import AlexandrDrmax
import lxml.html
#from BeautifulSoup import BeautifulSoup
import re


class Aaaleky(CrawlSpider):

	name='aaaleky'

	allowed_domains = ['www.aaaleky.cz']

	start_urls = [

			"http://www.aaaleky.cz/kategorie/sexualita-afrodiziaka-1",
			"http://www.aaaleky.cz/kategorie/caje-diety-energeticke-napoje-2",
			"http://www.aaaleky.cz/kategorie/volne-prodejne-pripravky-a-leciva-3",
			"http://www.aaaleky.cz/kategorie/oci-bryle-4",
			"http://www.aaaleky.cz/kategorie/deti-maminky-5",
			"http://www.aaaleky.cz/kategorie/stres-nervozita-spanek-6",
			"http://www.aaaleky.cz/kategorie/zdravotnicky-material-7",
			"http://www.aaaleky.cz/kategorie/obuv-8",
			"http://www.aaaleky.cz/kategorie/kosmetika-9",
			"http://www.aaaleky.cz/kategorie/prirodni-produkty-10",
			"http://www.aaaleky.cz/kategorie/ostatni-12",
			"http://www.aaaleky.cz/kategorie/vitaminy-mineraly-13",
			"http://www.aaaleky.cz/kategorie/zeny-resi-problemy-14",
			"http://www.aaaleky.cz/kategorie/muzi-resi-problemy-15",
			"http://www.aaaleky.cz/kategorie/zdravotni-technika-16",

	]

	def parse(self, response):
		for page in range(200):

			link = response.url +'?idvyrobce=-1&idpagezbozikats='+ str(page)
			request = Request(link, callback = self.parse_page)
			yield request

	def parse_page(self, response):
		item = AlexandrDrmax()
		hxs = HtmlXPathSelector(response)		
		price = hxs.xpath("//div[@class='item']/p[@class='item-czk']/strong/text()").extract()
		name = hxs.xpath("//div[@class='item']/h2[@class='item-h']/a/text()").extract()
		for p,n in zip(price, name):
			p = p.strip()
			p = (''.join([i for i in p if i.isdigit()]))
			p = p[0:int(len(p))-2]
			item['price'] = p
			item['name'] = n
			item['site_link'] = self.allowed_domains[0]

			#
			pattern = re.compile("(\d+)")
			d=re.sub(pattern,r' \1 ', n)
			d=[int(s) for s in d.split() if s.isdigit()]
			d= ' '.join(map(str, d))
			item['name_with_digits']=d.decode( 'unicode-escape' )

			yield item

