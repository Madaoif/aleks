# coding: utf-8
import requests
import os
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector, Selector
from parse2.items import AlexandrDrmax
import lxml.html
#from BeautifulSoup import BeautifulSoup
import re


class Benu(CrawlSpider):

	name='benu'

	allowed_domains = ['www.benu.cz']

	start_urls = [

			"https://www.benu.cz/mam-problem",
			"https://www.benu.cz/volne-prodejne-leky",
			"https://www.benu.cz/vyziva-a-doplnky-stravy",
			"https://www.benu.cz/vitaminy-a-mineraly",
			"https://www.benu.cz/caje-a-bylinky",
			"https://www.benu.cz/deti-a-maminky",
			"https://www.benu.cz/kosmetika-a-hygiena",
			"https://www.benu.cz/zdravotnicke-pomucky",
			"https://www.benu.cz/veterinarni-potreby",

	]

	domain = "https://www.benu.cz"

	def parse(self, response):

		for page in range(200):
			link = response.url +'?page='+ str(page)
			request = Request(link, callback = self.parse_page)
			yield request

	# def parse_page(self, response):

	# 	hxs = HtmlXPathSelector(response)
	# 	links = hxs.xpath("//div[@class='spc']/a/@href").extract()
	# 	for link in links:
	# 		request = Request(self.domain+link, callback = self.parse_detail_page)
	# 		yield request
		


	# def parse_detail_page(self, response):
	# 	item = AlexandrDrmax()
	# 	hxs = HtmlXPathSelector(response)
	# 	try:
	# 		#//h2[@class='title']/span[@class='name']/text()
	# 		name = hxs.xpath("//h1[@class='title itemreviewed']/text()").extract()[0]
	# 	except:
	# 		pass
	# 	try:
	# 		#//div[@class='products-list']/ul/li/div[@class="spc"]/form[@class="shop-box ajax"]/div[@class='in']/div[@class='av-price']/p[@class='price']/strong/text()
	# 		price = hxs.xpath("//span[@class='prices']/strong/text()").extract()[0]
	# 	except:
	# 		pass
	# 	try:
	# 		p = int(''.join([i for i in price if i.isdigit()]))
	# 		p = lxml.html.fromstring(str(p)).text_content().strip()
	# 		item['price'] = p
	# 		item['name'] = name
	# 		item['site_link'] = self.allowed_domains[0]

	# 		#
	# 		pattern = re.compile("(\d+)")
	# 		d=re.sub(pattern,r' \1 ', n)
	# 		d=[int(s) for s in d.split() if s.isdigit()]
	# 		d= ' '.join(map(str, d))
	# 		item['name_with_digits']=d.decode( 'unicode-escape' )

	# 		yield item
	# 	except:
	# 		pass


	def parse_page(self, response):
		item = AlexandrDrmax()
		hxs = HtmlXPathSelector(response)		
		price = hxs.xpath("//div[@class='products-list']/ul/li/div[@class='spc']/form[@class='shop-box ajax']/div[@class='in']/div[@class='av-price']/p[@class='price']/strong/text()").extract()
		name = hxs.xpath("//h2[@class='title']/span[@class='name']/text()").extract()
		for p,n in zip(price, name):
			#p = p.strip()
			p = (''.join([i for i in p if i.isdigit()]))
			#p = p[0:int(len(p))-2]
			item['price'] = p
			item['name'] = n
			item['site_link'] = self.allowed_domains[0]

			#
			pattern = re.compile("(\d+)")
			d=re.sub(pattern,r' \1 ', n)
			d=[int(s) for s in d.split() if s.isdigit()]
			d= ' '.join(map(str, d))
			item['name_with_digits']=d.decode( 'unicode-escape' )

			yield item
