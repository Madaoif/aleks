# coding: utf-8
import requests
import os
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector, Selector
from parse2.items import AlexandrDrmax
import lxml.html
#from BeautifulSoup import BeautifulSoup
import re

class Lekarna(CrawlSpider):

	name = 'lekarna'

	allowed_domains = ['www.lekarna.cz']

	start_urls = [

			"http://www.lekarna.cz/vyprodej/",
			"http://www.lekarna.cz/caje-diety-potraviny/",
			"http://www.lekarna.cz/deti-maminky/",
			"http://www.lekarna.cz/drogerie/",
			"http://www.lekarna.cz/elektro/",
			"http://www.lekarna.cz/kosmetika/",
			"http://www.lekarna.cz/leky-na-predpis/",
			"http://www.lekarna.cz/oci-kontaktni-cocky/",
			"http://www.lekarna.cz/prirodni-produkty/",
			"http://www.lekarna.cz/sex-sport/",
			"http://www.lekarna.cz/veterinarni-pripravky/",
			"http://www.lekarna.cz/vitaminy-mineraly/",
			"http://www.lekarna.cz/leky-volne-prodejne/",
			"http://www.lekarna.cz/zdravotnicky-material/",

	]

	def parse(self, response):
		for page in range(200):
			link = response.url +'?page='+ str(page)
			request = Request(link, callback = self.parse_page)
			yield request

	def parse_page(self, response):
		item = AlexandrDrmax()
		hxs = HtmlXPathSelector(response)
		price= hxs.xpath("//div[@class='productListing']/div/div/div[@class='rightSide']/p/strong[@class='orange']/text()").extract()
		name = hxs.xpath("//div[@class='productListing']/div/div/h2/a/@title").extract()
		for p,n in zip(price, name):
			p = p.strip()
			p = int(''.join([i for i in p if i.isdigit()]))	
			item['price'] = p
			item['name'] = n
			item['site_link'] = self.allowed_domains[0]

			#
			pattern = re.compile("(\d+)")
			d=re.sub(pattern,r' \1 ', n)
			d=[int(s) for s in d.split() if s.isdigit()]
			d= ' '.join(map(str, d))
			item['name_with_digits']=d.decode( 'unicode-escape' )

			yield item
