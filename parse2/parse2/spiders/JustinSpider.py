# coding: utf-8
import requests
import os
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector, Selector
from parse2.items import AlexandrDrmax
import lxml.html
from bs4 import BeautifulSoup
import re

class Drmax(CrawlSpider):

	name = 'drmax'

	allowed_domains = ['www.drmax.cz']

	start_urls = [

			"https://www.drmax.cz/bolest-a-nachlazeni/proti-bolesti",
			"https://www.drmax.cz/bolest-a-nachlazeni/proti-chripce-a-nachlazeni",
			"https://www.drmax.cz/bolest-a-nachlazeni/nosni-kapky-a-spreje",
			"https://www.drmax.cz/bolest-a-nachlazeni/poraneni-a-dezinfekce",
			"https://www.drmax.cz/leky-a-zdravotnicke-prostredky/chripka-nachlazeni",
			"https://www.drmax.cz/leky-a-zdravotnicke-prostredky/chripka-nachlazeni",
			"https://www.drmax.cz/leky-a-zdravotnicke-prostredky/chripka-nachlazeni",

	]

	def parse(self, response):

		for page in range(0,1000,20):
			link = response.url +'?offset='+ str(page)
			request = Request(link, callback = self.parse_page)
			yield request

	def parse_page(self, response):
		item = AlexandrDrmax()
		hxs = HtmlXPathSelector(response)
		page = BeautifulSoup(response.text)
		names = page.findAll("div",{"class":'product-item-title'})
		name = map(lambda x:x.find("a").get_text(), names)
		price = page.findAll("div",{"class":'product-item-price'})
		price = map(lambda x:x.find('span',{'class':'price'}), price)
		price = map(lambda x:x.get_text(), price)
		#name= hxs.xpath("//div[@class='product-item-title']/a/text()").extract()
		for p,n in zip(price, name):
			p = int(''.join([i for i in p if i.isdigit()]))
			item['price'] = p
			item['name'] = n

			#
			pattern = re.compile("(\d+)")
			d=re.sub(pattern,r' \1 ', n)
			d=[int(s) for s in d.split() if s.isdigit()]
			d= ' '.join(map(str, d))
			item['name_with_digits']=d.decode( 'unicode-escape' )


			print p, n
			item['site_link'] = self.allowed_domains[0]
			yield item


