# coding: utf-8
import requests
import os
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import HtmlXPathSelector, Selector
from parse2.items import AlexandrDrmax
import lxml.html
#from BeautifulSoup import BeautifulSoup
import re


class Pilulka(CrawlSpider):

	name='pilulka'

	allowed_domains = ['www.pilulka.cz']

	start_urls = [

			"http://www.pilulka.cz/volne-prodejne-leky",
			"http://www.pilulka.cz/chripka-a-nachlazeni-kasel",
			"http://www.pilulka.cz/vitaminy-a-doplnky-stravy",
			"http://www.pilulka.cz/oci-usi-nos-a-usta",
			"http://www.pilulka.cz/caje-a-potraviny",
			"http://www.pilulka.cz/matka-a-dite-tehotenstvi",
			"http://www.pilulka.cz/kosmetika-a-drogerie",
			"http://www.pilulka.cz/sport-a-zivotosprava",
			"http://www.pilulka.cz/zdravotnicky-material",
			"http://www.pilulka.cz/vyprodej",

	]

	def parse(self, response):

		for page in range(200):
			link = response.url +'?page='+ str(page)
			request = Request(link, callback = self.parse_page)
			yield request

	def parse_page(self, response):
		item = AlexandrDrmax()
		hxs = HtmlXPathSelector(response)
		price = hxs.xpath("//p[@class='price']/span/strong/text()").extract()
		name = hxs.xpath("//h2[@class='title']/a/span[@class='nameRating']/span[@class='name']/text()").extract()
		for p,n in zip(price, name):
			p = p.strip()
			p = int(''.join([i for i in p if i.isdigit()]))
			item['price'] = p
			item['name'] = n
			item['site_link'] = self.allowed_domains[0]

			#
			pattern = re.compile("(\d+)")
			d=re.sub(pattern,r' \1 ', n)
			d=[int(s) for s in d.split() if s.isdigit()]
			d= ' '.join(map(str, d))
			item['name_with_digits']=d.decode( 'unicode-escape' )

			yield item
