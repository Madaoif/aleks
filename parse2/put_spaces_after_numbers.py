import re
def put_spaces_after_numbers(string):
    pattern = re.compile("(\d+)")
    return re.sub(pattern,r' \1 ', string)

def leave_only_alphanumeric(string):
   pat = re.compile('[\W]+', re.UNICODE)
   return re.sub(pat,' ',string)

def list_to_string(L):
	if len(L)==0:
		return''
	return L[0]+list_to_string(L[1:])

#print put_spaces_after_numbers('solmucol orm.pas.24x100mg')
#print put_spaces_after_numbers('leave_only_alphanumeric')
print list_to_string([u'solmucol orm.pas.24x100mg '])