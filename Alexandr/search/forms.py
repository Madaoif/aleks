# -*- coding: utf-8 -*-
from search.models import Alex
from django import forms
#haystack
from haystack.forms import ModelSearchForm
from haystack.query import SearchQuerySet

class MySearchForm(ModelSearchForm):


    def search(self):
        if not self.is_valid():
            return self.no_query_found()
        if not self.cleaned_data.get('q'):
            return self.no_query_found()
        sqs = SearchQuerySet.autocomplete(name=self.cleaned_data['q'])
        if self.load_all:
            sqs = sqs.load_all()
        return sqs