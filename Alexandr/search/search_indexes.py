import datetime
from haystack import indexes
from search.models import Alex

class AlexIndex(indexes.SearchIndex, indexes.Indexable):
	text = indexes.EdgeNgramField(document = True, use_template = True)
	created_date = indexes.DateTimeField(model_attr='created_date')
	name_2 = indexes.CharField(model_attr='name')
	price = indexes.DecimalField(model_attr='price')
	site_link = indexes.CharField(model_attr='site_link')
	name = indexes.EdgeNgramField(model_attr='name')
	name_with_digits = indexes.CharField(model_attr='name_with_digits')


	def get_model(self):
		return Alex

	def index_queryset(self, using=None):
		return self.get_model().objects.all()