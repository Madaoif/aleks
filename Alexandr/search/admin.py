from django.contrib import admin
from search.models import Alex


class AlexAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'created_date', 'site_link', 'serialization')
    search_fields = ('name',)

admin.site.register(Alex, AlexAdmin)
