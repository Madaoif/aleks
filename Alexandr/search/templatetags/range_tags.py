from django.template.defaulttags import register
from django import template
import re
from django.template.defaultfilters import stringfilter
register = template.Library()

@register.filter(name='sss')
@stringfilter
def sum (n):
    #n='169 137'
	pattern = re.compile("(\d+)")
	d=re.sub(pattern,r' \1 ', n)
	d=[int(s) for s in d.split() if s.isdigit()]
	#d= ' '.join(map(str, d))
	return str(sum(d))




@register.filter(name='lookup')
def cut(value, arg):
    return value[arg]
@register.filter
def customFilter(value):
    return value / 60.0 * 0.16