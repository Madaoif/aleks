#Alexandr app
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from .models import Alex
from search.forms import MySearchForm
from django.http import HttpResponse
#haystack
from haystack.generic_views import SearchView
from haystack.query import SearchQuerySet
#third party libs
import json
import numpy as np
import re
from django.db.models import Q
from django.db.models import Count
import operator
import itertools
from django.db.models import Avg

class MySearchView(SearchView):

	template = 'search/search.html'
	queryset = SearchQuerySet().all()               
	load_all = True
	prices = None
	def put_spaces_after_numbers(self, string):
		pattern = re.compile("(\d+)")
		return re.sub(pattern,r' \1 ', string)

	def list_to_string(self, L):
		if len(L)==0:
			return''
		return L[0]+list_to_string(L[1:])

	def get(self, request,  *args, **kwargs):

		try:

			prices = self.queryset.filter(name = self.request.GET.get('q'))
			price = []

			for i in prices:
				price.append(float(i.price))
			self.prices = price
				#print i

		except:
			pass
		return super(MySearchView, self).get(request, *args, **kwargs)


	def get_context_data(self, *args, **kwargs):
		context = super(MySearchView, self).get_context_data(**kwargs)
		context['queryset'] = self.queryset.filter(name = self.request.GET.get('q'))
		m = self.queryset.filter(name = self.request.GET.get('q')).order_by('name_with_digits')
		n = operator.attrgetter('name_with_digits')
		k = [{k:list(g)} for k,g in itertools.groupby(m,n)]
		d ={}
		#m=[]
		for i in k:
			
			
			for j,c in i.iteritems():
				
				links= len([f.site_link for f in c])
				
				avarage2 =  np.average(np.asarray([float(f.price) for f in c]))
				amin=np.amin(np.asarray([float(f.price) for f in c]))
				amax=np.amax(np.asarray([float(f.price) for f in c]))
				print [float(f.price) for f in c]
				c.append(links)
				c.append(amax)
				c.append(amin)
				c.append(avarage2)
				#print c
				#for f in c:
					#print f
				#print context['avarage2']
				#print np.amin(np.asarray([float(f.price) for f in c]))
				#print np.amax(np.asarray([float(f.price) for f in c]))
					#p_av=np.average(np.asarray(float(f.price)))

					#print p_av
				d[j] = [i for i in c]
		#context['avarage2'] = m
			

				


				#amin2=np.amin(np.asarray([float(f.price) for f in c]))
				#amax2=np.amax(np.asarray([float(f.price) for f in c]))
				#print amax2

		#for i in context['queryset']:
			#print i.values('name').annotate(dcount=Count('name_with_digits'))[0]
		#print d

		#for z in d:
			#print d[z]
		context['test'] = d
		try:
			context['avarage'] = np.average(np.asarray(self.prices))
			#print context['avarage']
			context['minimal'] =  np.amin(np.asarray(self.prices))
			context['maximum'] =  np.amax(np.asarray(self.prices))
		except:
			pass
		return context

	# def autocomplete(request):
	# 	print "Hello World"
	# 	sqs = SearchQuerySet().autocomplete(name=request.GET.get('q', ''))[:5]
	# 	suggestions = [result.title for result in sqs]
	# 	the_data = json.dumps({
	# 		'results': suggestions
	# 	})
		
	# 	return HttpResponse(the_data, content_type='application/json')














    
